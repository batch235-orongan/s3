const express = require("express");
const port = 4000;
const app = express();

// middleware
app.use(express.json());

// mock database
let users = [
	{
		name: "Jojo Joestar",
		age: 25,
		username: "Jojo"
	},
	{
		name: "Dio Brando",
		age: 23,
		username: "Dio"
	},
	{
		name: "Jotaro Kujo",
		age: 28,
		username: "Jotaro"
	}

]

// Activity

let artists = [
	{
		name: "Arthur Nery",
		songs: ["Pagsamo", "Isa Lang"],	
		album: "Letters Never Sent",
		isActive: true
	},
	{
		name: "Zack Tabudlo",
		songs: ["Heart Can't Lose", "Lost"],
		album: "Episode",
		isActive: true
	},
	{
		name: "Ben&Ben",
		songs: ["Pagtingin", "Fall"],
		album: "Limasawa Street",
		isActive: true
	}
]

// [SECTION] Routes and Controllers for USERS

app.get("/users", (req, res) => {
	console.log(users);
	return res.send(users);
});

app.post("/users", (req, res) => {

	// add simple if statement that if the request body does not have a property name, we will send a message along with a 400 http status code (Bad Request)
	// hasOwnProperty() returns a boolean if the property name passed exists or does not exxist in the given object.
	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}

	// mini activity

	if(!req.body.hasOwnProperty("age")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter AGE"
		})
	}
})

// Activity section

app.get("/artists", (req, res) => {
	console.log(artists);
	return res.send(artists);
});

app.post("/artists", (req, res) => {

	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter name"
		})
	}
	if(!req.body.hasOwnProperty("songs")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter songs"
		})
	}
	if(!req.body.hasOwnProperty("album")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter album"
		})
	}
	// if(!req.body.hasOwnProperty("isActive")){
	// 	return res.status(400).send({
	// 		error: "Bad Request - missing required parameter isActive"
	// 	})
	// }
	if(!req.body.isActive){
		return res.status(400).send({
			error: "Bad Request - isActive property is equal to false"
		})
	}

})

// port listener
app.listen(port, () => console.log(`Server is running at port ${port}`));