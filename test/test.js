const chai = require("chai");
const { assert } = require("chai");

// import and use chai-http to allow chai to send request to our server
const http = require("chai-http");
chai.use(http);

describe("API Test Suite for users", () => {

	it("Test API get users is running", (done) => {

		// request() method is used from chai to create an http request given to the server
		// get("/endpoint") method is used to run/ access a get method route.
		// end() method is used to access the response from the route. It has anonymous function as an argument that recieves 2 objects, the err or the response.
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {
			// isDefined() is assertion that given data is not undefined. It's like a shortcut to .notEqual(typeof dat, undefined)
			assert.isDefined(res);
			// done() method is used to tell chai-http when the test is done.
			done();
		})
	})


	it("Test API get users returns an array", (done) => {
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {

			//res.body contains the bosy of the response. Thd data sent from res.send()
			// isArray() is an assertion that the given data is an array
			console.log(res.body);
			assert.isArray(res.body);
			done();
		})
	})
	it("Test API get users array first object username is Jojo", (done) => {

		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {

			assert.equal(res.body[0].username, "Jojo");
			done();
		})
	})

	it("Test API get users array last object item is not undefined", (done) => {

		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {

			assert.notEqual(res.body[2], undefined);
			// assert.notEqual(res.body[res.body.length -1], undefined);
			done();
		})
	})

	it("Test API post user returns 400 if no name", (done) => {

		// post() which is used by chai http to access a post method route
		// type() which is used to tell chai that request bosy is going to be stringified as json
		// send() is used to send the request body
		chai.request("http://localhost:4000")
		.post("/users")
		.type("json")
		.send({
			// name: "jin",
			age: 30,
			username: "jin92"
		})
		.end((err, res) => {
			// console.log(res.status);
			assert.equal(res.status, 400)
			done();
		})
	})

	// mini activity

	it("Test API post user returns 400 if no age", (done) => {

		chai.request("http://localhost:4000")
		.post("/users")
		.type("json")
		.send({
			// name: "jin",
			// age: 30,
			username: "jin92"
		})
		.end((err, res) => {
			// console.log(res.status);
			assert.equal(res.status, 400)
			done();
		})
	})
})

// Activity

describe("API Test Suite for artists", () => {
// a.
	it("Test API get artists is running", (done) => {
		chai.request("http://localhost:4000")
		.get("/artists")
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
// b.
	it("Test API get artists songs properties of first object is an array", (done) => {
		chai.request("http://localhost:4000")
		.get("/artists")
		.end((err, res) => {
			// console.log(res.body[0].songs);
			assert.isArray(res.body[0].songs);
			done();
		})
	})
// c.
	it("Test API post artists returns 400 if no name", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			// name: "Moira Dela Torre",
			songs: ["Pahinga", "Ikaw Pa Rin"],
			album: "Patawad",
			isActive: true
		})
		.end((err, res) => {
			// console.log(res.status);
			assert.equal(res.status, 400)
			done();
		})
	})
// d.
	it("Test API post artists returns 400 if no songs", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			name: "Moira Dela Torre",
			// songs: ["Pahinga", "Ikaw Pa Rin"],
			album: "Patawad",
			isActive: true
		})
		.end((err, res) => {
			// console.log(res.status);
			assert.equal(res.status, 400)
			done();
		})
	})
// e.	
	it("Test API post artists returns 400 if no album", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			name: "Moira Dela Torre",
			songs: ["Pahinga", "Ikaw Pa Rin"],
			// album: "Patawad",
			isActive: true
		})
		.end((err, res) => {
			// console.log(res.status);
			assert.equal(res.status, 400)
			done();
		})
	})
// f.	
	it("Test API post artists is Active is not equal to true", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			name: "Moira Dela Torre",
			songs: ["Pahinga", "Ikaw Pa Rin"],
			album: "Patawad",
			isActive: false
		})
		.end((err, res) => {
			// console.log(res.status);
			assert.equal(res.status, 400)
			done();
		})
	})
})